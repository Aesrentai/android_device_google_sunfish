# Build necessary vendor packages
PRODUCT_PACKAGES += \
    vendor.qti.hardware.capabilityconfigstore@1.0 \
    libplatformconfig \

# Camera
PRODUCT_PACKAGES += \
    Snap \

# GMS
WITH_GMS_FI := true

# Lights
PRODUCT_PACKAGES += \
    hardware.google.light@1.0.vendor \

# Properties
TARGET_VENDOR_PROP := $(LOCAL_PATH)/vendor.prop

# Trust HAL
PRODUCT_PACKAGES += \
    vendor.lineage.trust@1.0-service \
    
# Vendor Security Patch Level
VENDOR_SECURITY_PATCH := "2020-12-05"

